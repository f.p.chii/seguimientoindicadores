/**
 * 
 * Crear una nueva app que me permita hacer seguimiento de los valores de indicadores monetarios.

Menú
Actualizar datos (Escribe nuevo archivo)
Promedio últimos 5 archivos 
Minimo y maximo últimos 5 archivos
Salir

Observaciones
Utilice la API mi indicador (midicador.cl
Haga uso de promesas
La persistencia de la aplicación debe ser en archivos JSON.
Divida la aplicación en los siguientes módulos:
Data API
Files API
 */
require( 'console-stamp' )( console );

const readline = require('readline');
var data = require('./dataAPI');
var file = require('./fileAPI');

var sc = readline.createInterface(
    {
        input: process.stdin,
        output: process.stdout
    }
);

function menu(){

    
    console.log('-------Menú-----');
    console.log('1. Actualizar datos (Escribir nuevo archivo)');  
    console.log('2. Promediar datos (5 últimos archivos)');
    console.log('3. Mínimo y máximo (5 últimos archivos)');
    console.log('4. Salir');
    sc.question('Elija una opción\n', function opcion(opc){
        switch (opc) {
            case '1': file.actualizarDatos(); menu(); break;
            case '2': data.calcularPromedio(); menu(); break;
            case '3': data.minimoYMaximo(); menu(); break;
            case '4':
                console.log('Cerrando la aplicación...');
                sc.close();
                process.exit(0);
                break;
            default: 
            console.log('Elija una opción válida');
            menu();
                break;
        }
    })


}




menu();