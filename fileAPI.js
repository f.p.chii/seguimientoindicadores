const fetch = require('node-fetch');
const fs = require('fs');
const data= require('./dataAPI.js');

const stat = resp =>
{
    if(resp.status >= 200 && resp.status < 300)
    {
        return Promise.resolve(resp);
    }

    return Promise.reject(new Error(resp.statusText));
};

const obtenerJson = resp => {
    return resp.json();
}

function leer ()
{
    return fetch('https://mindicador.cl/api')
    .then(stat)
    .then(obtenerJson)
};

function actualizar(){
    

    //lee los datos del archivo
    const p1 = getFile('./datos.json')
    .then(r=>JSON.parse(r))

    //.then(guardar);

    //lee los datos de internet
    const p2 = leer()


    //los guarda
    Promise.all([p1,p2])
    .then(insertarElelementoEnElArray)
    .then(guardar)
    .catch(err => console.error(err));

};

const insertarElelementoEnElArray = function (objetoRaro) {
    var arr = objetoRaro[0];    
    var e = objetoRaro [1];
    arr.push(e);    
    return arr;
}
function getFile(fileName){
    return new Promise((resolve, reject) =>
    {
        fs.readFile(fileName,'utf8', (err, data) =>
        {
            if (err)
            {
                reject(err);
                return;
            }            
            resolve(data);
        })
    });
}

var guardar = function (datos) {

    fs.writeFile('./datos.json', JSON.stringify(datos,null,"\t"), function(err)
    {
        if (err) throw err;
    });
}

    

    



module.exports = {
    actualizarDatos :  actualizar,
    recuperarDatos : getFile,
};